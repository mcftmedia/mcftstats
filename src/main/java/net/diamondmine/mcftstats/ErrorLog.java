package net.diamondmine.mcftstats;

import static net.diamondmine.mcftstats.McftStats.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public final class ErrorLog {
	protected McftStats plugin;
	private LinkedHashMap<String, Object> output = new LinkedHashMap<String, Object>();
	private String file;

	public ErrorLog(String file, McftStats plugin) {
		this.plugin = plugin;
		this.file = file;

		if (!(new File(file).exists())) {
			save();
		}
	}

	// Save data from LinkedHashMap to file
	public void save() {
		BufferedWriter bw = null;
		try {
			// Construct the BufferedWriter object
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
			bw.write("# McftStats Error Log");
			bw.newLine();

			// Save all the properties one at a time, only if there's data to
			// write
			if (output.size() > 0) {
				// Grab all the entries and create an iterator to run through
				// them all
				Set<?> set = output.entrySet();
				Iterator<?> i = set.iterator();

				// While there's data to iterate through..
				while (i.hasNext()) {
					// Map the entry and save the key and value as variables
					Map.Entry<?, ?> me = (Map.Entry<?, ?>) i.next();

					// Write the key and value pair as key=value
					bw.write(me.getKey().toString() + ": " + me.getValue().toString());
					bw.newLine();
				}
			}
		} catch (FileNotFoundException ex) {
			log("Couldn't find file " + file, "warning");
			return;
		} catch (IOException ex) {
			log("Unable to save " + file, "warning");
			return;
		} finally {
			// Close the BufferedWriter
			try {
				if (bw != null) {
					bw.close();
				}
			} catch (IOException ex) {
				log("Unable to save " + file, "warning");
			}
		}
	}

	// Set output value as a string
	public void setString(String key, String value) {
		output.put(key, value);
	}

	// Set output value as an integer
	public void setInt(String key, int value) {
		output.put(key, String.valueOf(value));
	}

	// Set output value as a double
	public void setDouble(String key, double value) {
		output.put(key, String.valueOf(value));
	}

	// Set output value as a long
	public void setLong(String key, long value) {
		output.put(key, String.valueOf(value));
	}

	// Set output value as a float
	public void setFloat(String key, float value) {
		output.put(key, String.valueOf(value));
	}

	// Set output value as a boolean
	public void setBoolean(String key, boolean value) {
		output.put(key, String.valueOf(value));
	}
}