package net.diamondmine.mcftstats;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class McftStats extends JavaPlugin {
	public static final Logger logger = Logger.getLogger("Minecraft");
	public LinkedHashMap<String, Object> config;
	public HashMap<String, Property> users = new HashMap<String, Property>();
	public HashMap<String, Integer> actions = new HashMap<String, Integer>();
	public String df;
	public String uf;
	public String players;
	public String logs;
	public Database db;
	public Property conf;
	private Repeater runner;
	private Boolean disabled = false;
	public String table = "";

	@Override
	public final void onDisable() {
		if (!users.isEmpty()) {
			try {
				runner.out();
			} catch (Exception e) {
				db.update("logged=0");
			}
		}

		PluginDescriptionFile pdfFile = getDescription();
		String version = pdfFile.getVersion();
		log("Version " + version + " is disabled!", "info");
	}

	@Override
	public void onEnable() {
		df = getDataFolder().toString();
		uf = getServer().getUpdateFolder();
		players = df + "/players";
		logs = df + "/logs";
		String configFile = df + "/config.txt";
		mkDirs(df, uf, players, logs);

		if (!(new File(configFile).exists())) {
			try {
				new File(configFile).createNewFile();
			} catch (Exception e) {
				log("Couldn't create config file. Make sure your plugins directory has write access.", "severe");
			}
			conf = new Property(configFile, this);
			conf.setString("admin", "");
			conf.setString("host", "");
			conf.setString("database", "");
			conf.setString("username", "");
			conf.setString("password", "");
			conf.setString("#0", "Both oldTable and newTable are optional");
			conf.setString("oldTable", "");
			conf.setString("newTable", ""); // Only one that truly matters,
											// oldTable is reference
			conf.setInt("actions", 32);
			conf.setInt("updateRate", 90); // Time in seconds
			// Now set option tracking options
			conf.setString("#1", "Optional things to track. True = track, False = don't track");
			conf.setBoolean("trackIP", true);
			conf.setBoolean("trackBroken", true);
			conf.setBoolean("trackPlaced", true);
			conf.setBoolean("trackDeaths", true);
			conf.setBoolean("trackMonsterKills", true);
			conf.setBoolean("trackPlayerKills", true);
			conf.setBoolean("trackDistanceWalked", true);
			conf.save();
			log("Your config isn't set up. Creating one and disabling McftStats.", "severe");
			disabled = true;
		} else {
			// File exists, check if the database info is there
			conf = new Property(configFile, this);
			if (conf.isEmpty("host") || conf.isEmpty("username")) {
				log("Your database settings aren't set. Disabling McftStats.", "severe");
				disabled = true;
			} else {
				// Database info exists, build a temporary config in the newest
				// format
				LinkedHashMap<String, Object> tmp = new LinkedHashMap<String, Object>();
				tmp.put("admin", conf.getString("admin"));
				tmp.put("host", conf.getString("host"));
				tmp.put("database", conf.getString("database"));
				tmp.put("username", conf.getString("username"));
				tmp.put("password", conf.getString("password"));
				tmp.put("#0", "Both oldTable and newTable are optional");
				tmp.put("oldTable", conf.getString("oldTable"));
				tmp.put("newTable", conf.getString("newTable"));
				tmp.put("actions", conf.getInt("actions"));
				tmp.put("updateRate", conf.getInt("updateRate"));
				tmp.put("#1", "Optional things to track. True = track, False = don't track");
				tmp.put("trackIP", conf.getBoolean("trackIP"));
				tmp.put("trackBroken", conf.getBoolean("trackBroken"));
				tmp.put("trackPlaced", conf.getBoolean("trackPlaced"));
				tmp.put("trackDeaths", conf.getBoolean("trackDeaths"));
				tmp.put("trackMonsterKills", conf.getBoolean("trackMonsterKills"));
				tmp.put("trackPlayerKills", conf.getBoolean("trackPlayerKills"));
				tmp.put("trackDistanceWalked", conf.getBoolean("trackDistanceWalked"));
				tmp.put("revised", ((conf.keyExists("revised")) ? conf.getBoolean("revised") : false));
				// Check if old config matches new config format
				if (!conf.match(tmp)) {
					conf.rebuild(tmp);
				}
				// Now config = tmp since it'll always be up-to-date
				config = tmp;
				// If newTable isn't empty
				String oldTable;
				if (!conf.isEmpty("newTable")) {
					// If oldTable is empty, use netstats otherwise use oldTable
					// to be renamed
					oldTable = (conf.isEmpty("oldTable")) ? "netstats" : conf.getString("oldTable");
					String newTable = (conf.getString("newTable").equalsIgnoreCase("netstats") && !oldTable.equalsIgnoreCase("netstats")) ? "netstats" : conf.getString("newTable");
					table = newTable;
					db = new Database(conf.getString("host"), conf.getString("database"), conf.getString("username"), conf.getString("password"), this);
					db.queries("CREATE TABLE `" + table + "` LIKE `" + oldTable + "`;INSERT INTO `" + table + "` SELECT * FROM `" + oldTable + "`;DROP TABLE `" + oldTable + "`;");
					config.put("newTable", table);
					config.put("oldTable", ((table.equalsIgnoreCase("netstats")) ? "" : table));
					conf.setString("oldTable", ((table.equalsIgnoreCase("netstats")) ? "" : table));
					conf.setString("newTable", "");
					conf.save();
				} else {
					oldTable = (conf.isEmpty("oldTable")) ? "netstats" : conf.getString("oldTable");
					table = oldTable;
					db = new Database(conf.getString("host"), conf.getString("database"), conf.getString("username"), conf.getString("password"), this);
					config.put("oldTable", table);
				}
				// Do a one time revision if null or false
				if ((config.get("revised") == null) || !(new Boolean((Boolean) config.get("revised")))) {
					db.update("total=total/1000");
					conf.setBoolean("revised", true);
					conf.save();
				}
				// END REVISION
				// First, the plugin is either reloading or is starting up, so
				// set all users to being logged off
				db.update("logged=0");
				// If the plugin is just reloading, we need to set all online
				// players back to being online
				for (Player p : getServer().getOnlinePlayers()) {
					db.update("logged=1 WHERE player='" + p.getName() + "';");
				}
			}
		}
		if (!disabled) {
			runner = new Repeater(this);

			getServer().getPluginManager().registerEvents(new Listeners(this), this);

			if ((Integer) config.get("updateRate") > 0) {
				int rate = ((Integer) config.get("updateRate") * 20);
				getServer().getScheduler().scheduleSyncRepeatingTask(this, runner, rate, rate);
			}
		} else {
			getPluginLoader().disablePlugin(this);
		}

		PluginDescriptionFile pdfFile = getDescription();
		String version = pdfFile.getVersion();
		log("Version " + version + " enabled", "info");
	}

	private void mkDirs(String... d) {
		for (String f : d) {
			new File(f).mkdir();
		}
	}

	/**
	 * Sends a message to the logger.
	 * 
	 * @param s
	 *            The message to send
	 * @param type
	 *            The level (info, warning, severe)
	 * @since 1.0.0
	 */
	public static void log(final String s, final String type) {
		String message = "[McftStats] " + s;
		String t = type.toLowerCase();
		if (t != null) {
			boolean info = t.equals("info");
			boolean warning = t.equals("warning");
			boolean severe = t.equals("severe");
			if (info) {
				logger.info(message);
			} else if (warning) {
				logger.warning(message);
			} else if (severe) {
				logger.severe(message);
			} else {
				logger.info(message);
			}
		}
	}
}
