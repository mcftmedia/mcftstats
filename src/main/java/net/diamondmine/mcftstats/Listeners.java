package net.diamondmine.mcftstats;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;

public class Listeners implements Listener {
	public class PlayerAttack {
		Player lastAttackPlayer;
		long lastAttackTime;

		public PlayerAttack(Player attacker) {
			setAttack(attacker);
		}

		public long attackTimeAgo() {
			return lastAttackTime > 0 ? System.currentTimeMillis() - lastAttackTime : 0;
		}

		public Player getAttacker() {
			return lastAttackPlayer;
		}

		public final void setAttack(Player attacker) {
			lastAttackPlayer = attacker;
			lastAttackTime = System.currentTimeMillis();
		}
	}

	private int action;
	private HashMap<String, Integer> actions;
	protected HashMap<Integer, PlayerAttack> attacks = new HashMap<Integer, PlayerAttack>();
	private LinkedHashMap<String, Object> config;
	protected long damageTimeThreshold = 500;
	protected Database db;
	private String players;
	protected McftStats plugin;

	private HashMap<String, Property> users;

	public Listeners(McftStats plugin) {
		this.plugin = plugin;
		this.players = plugin.players;
		this.users = plugin.users;
		this.config = plugin.config;
		this.actions = plugin.actions;
		this.action = (Integer) config.get("actions");
		this.db = plugin.db;
	}

	void entDamage(Entity victim, Entity damager) {
		if (damager instanceof Player) {
			newAttack(victim, (Player) damager);
		}
	}

	void entDeath(Entity victim, Player damager) {
		if (damager != null) {
			long now = System.currentTimeMillis();
			if ((victim instanceof Monster) && ((Boolean) config.get("trackMonsterKills"))) {
				String name = damager.getName();
				// Player killed a monster, save it to player's stats file
				Property prop = users.get(name);
				if (!users.containsKey(name)) {
					users.put(name, new Property(players + '/' + name + ".stats", plugin));
					actions.put(name, (action / 2));
				}
				String sql = "";
				int count = actions.get(name) + 1;
				prop.inc("mobsKilled");
				if (count == action) {
					// Update database
					sql += (prop.getInt("broken") > 0) ? "broken=broken+" + prop.getInt("broken") + ", " : "";
					sql += (prop.getInt("placed") > 0) ? "placed=placed+" + prop.getInt("placed") + ", " : "";
					sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
					sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
					sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
					sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
					sql += "seen=" + now + ", total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + " WHERE player='" + name + "';";
					plugin.db.update(sql);
					actions.put(name, 0);
					// Reset everything (not time based) in property file since
					// we just updated the DB
					prop.setInt("broken", 0);
					prop.setInt("placed", 0);
					prop.setInt("deaths", 0);
					prop.setInt("mobsKilled", 0);
					prop.setInt("playersKilled", 0);
					prop.setDouble("distance", 0);
					prop.setLong("seen", now);
					prop.setLong("total", 0);
					prop.save();
				} else {
					prop.setLong("total", prop.getLong("total") + (now - prop.getLong("seen")));
					prop.setLong("seen", now);
					prop.save();
					actions.put(name, count);
				}
			} else if ((victim instanceof Player) && ((Boolean) config.get("trackPlayerKills"))) {
				String name = damager.getName();
				// Player killed a player, save it to player's stats file
				if (!users.containsKey(name)) {
					users.put(name, new Property(players + '/' + name + ".stats", plugin));
					actions.put(name, (action / 2));
				}
				Property prop = users.get(name);
				String sql = "";
				int count = actions.get(name) + 1;
				prop.inc("playersKilled");
				if (count == action) {
					// Update database
					sql += (prop.getInt("broken") > 0) ? "broken=broken+" + prop.getInt("broken") + ", " : "";
					sql += (prop.getInt("placed") > 0) ? "placed=placed+" + prop.getInt("placed") + ", " : "";
					sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
					sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
					sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
					sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
					sql += "seen=" + now + ", total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + " WHERE player='" + name + "';";
					plugin.db.update(sql);
					actions.put(name, 0);
					// Reset everything (not time based) in property file since
					// we just updated the DB
					prop.setInt("broken", 0);
					prop.setInt("placed", 0);
					prop.setInt("deaths", 0);
					prop.setInt("mobsKilled", 0);
					prop.setInt("playersKilled", 0);
					prop.setDouble("distance", 0);
					prop.setLong("seen", now);
					prop.setLong("total", 0);
					prop.save();
				} else {
					prop.setLong("total", prop.getLong("total") + (now - prop.getLong("seen")));
					prop.setLong("seen", now);
					prop.save();
					actions.put(name, count);
				}
			}
		}
	}

	public void newAttack(Entity attacker, Player player) {
		if (!attacks.containsKey(attacker.getEntityId())) {
			if (player != null) {
				attacks.put(attacker.getEntityId(), new PlayerAttack(player));
			}
		} else {
			attacks.get(attacker.getEntityId()).setAttack(player);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent e) {
		// If the event wasn't canceled by another plugin
		if (!e.isCancelled()) {
			String name = e.getPlayer().getName();
			if (!users.containsKey(name)) {
				// They reloaded the plugins, time to re-set the player property
				// files
				users.put(name, new Property(players + '/' + name + ".stats", plugin));
				actions.put(name, (action / 2));
			}
			Property prop = users.get(name);
			int count = actions.get(name) + 1;
			prop.inc("broken"); // Add 1 to broken
			if (count == action) {
				long now = System.currentTimeMillis();
				String sql = "";
				sql += "broken=broken+" + prop.getInt("broken") + ", ";
				sql += (prop.getInt("placed") > 0) ? "placed=placed+" + prop.getInt("placed") + ", " : "";
				sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
				sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
				sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
				sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
				sql += "seen=" + now + ", ";
				sql += "total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + " WHERE player='" + name + "';";
				db.update(sql);
				// Reset data data back to nothing except enter and total
				prop.setInt("broken", 0);
				prop.setInt("placed", 0);
				prop.setInt("deaths", 0);
				prop.setInt("mobsKilled", 0);
				prop.setInt("playersKilled", 0);
				prop.setDouble("distance", 0);
				prop.setLong("seen", now);
				prop.setLong("total", 0);
				prop.save();
				// Reset watched actions back to 0 (zero)
				actions.put(name, 0);
			} else {
				// Update timestamp
				long now = System.currentTimeMillis();
				prop.setLong("total", prop.getLong("total") + (now - prop.getLong("seen")));
				prop.setLong("seen", now);
				prop.save();
				actions.put(name, count);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockPlace(BlockPlaceEvent e) {
		// If the event wasn't canceled by another plugin
		if (!e.isCancelled()) {
			String name = e.getPlayer().getName();
			if (!users.containsKey(name)) {
				// Plugin is reset, make sure to re-set the property files
				users.put(name, new Property(players + '/' + name + ".stats", plugin));
				actions.put(name, (action / 2));
			}
			Property prop = users.get(name);
			int count = actions.get(name) + 1;
			prop.inc("placed");
			if (count == action) {
				long now = System.currentTimeMillis();
				String sql = "";
				sql += "placed=placed+" + prop.getInt("placed") + ", ";
				sql += (prop.getInt("broken") > 0) ? "broken=broken+" + prop.getInt("broken") + ", " : "";
				sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
				sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
				sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
				sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
				sql += "seen=" + now + ", ";
				sql += "total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + " WHERE player='" + name + "';";
				db.update(sql);
				// Reset data data back to nothing except enter and total
				prop.setInt("broken", 0);
				prop.setInt("placed", 0);
				prop.setInt("deaths", 0);
				prop.setInt("mobsKilled", 0);
				prop.setInt("playersKilled", 0);
				prop.setDouble("distance", 0);
				prop.setLong("seen", now);
				prop.setLong("total", 0);
				prop.save();
				// Reset watched actions back to 0 (zero)
				actions.put(name, 0);
			} else {
				// Update timestamp
				long now = System.currentTimeMillis();
				prop.setLong("total", prop.getLong("total") + (now - prop.getLong("seen")));
				prop.setLong("seen", now);
				prop.save();
				actions.put(name, count);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDamage(EntityDamageEvent e) {
		if (!e.isCancelled()) {
			if (e instanceof EntityDamageByEntityEvent) {
				Entity attacker = ((EntityDamageByEntityEvent) e).getDamager();
				entDamage(e.getEntity(), attacker);
			} else {
				// Entity fell, was burned, is drowning, or suffocating
				newAttack(e.getEntity(), null);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityDeath(EntityDeathEvent e) {
		Entity entity = e.getEntity();
		if (entity instanceof Player) {
			String name = ((Player) entity).getName();
			if (!users.containsKey(name)) {
				users.put(name, new Property(players + '/' + name + ".stats", plugin));
			}
			// It was a player who died, add that to their stats
			users.get(name).inc("deaths");
		}
		// An entity just died, if it was attacked (didn't fall, drown, or burn
		// to death)
		if (attacks.containsKey(entity.getEntityId())) {
			// and was attacked within a half-second ago
			if (attacks.get(entity.getEntityId()).attackTimeAgo() <= damageTimeThreshold) {
				// give killer credit for the kill
				entDeath(entity, attacks.get(entity.getEntityId()).getAttacker());
			}
			// remove all recorded attacks to this entity (garbage collection)
			attacks.remove(entity.getEntityId());
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent e) {
		long now = System.currentTimeMillis(); // In seconds
		InetSocketAddress IP = e.getPlayer().getAddress();
		String[] ips = IP.toString().split("/");
		String ip = ips[1].replace(":" + IP.getPort(), "");
		String name = e.getPlayer().getName();
		Property prop;
		String sql = "";
		String statfile = players + '/' + name + ".stats";
		if (!(new File(statfile)).exists()) {
			users.put(name, new Property(statfile, plugin));
			plugin.actions.put(name, 0);
			prop = users.get(name);
			prop.setLong("enter", 0);
			prop.setLong("seen", 0);
			prop.setLong("total", 0);
			prop.setInt("broken", 0);
			prop.setInt("placed", 0);
			prop.setInt("deaths", 0);
			prop.setInt("mobsKilled", 0);
			prop.setInt("playersKilled", 0);
			prop.setDouble("distance", 0);
			prop.save();
		} else {
			users.put(name, new Property(statfile, plugin));
			plugin.actions.put(name, 0);
			prop = users.get(name);
		}
		if (prop.getInt("broken") > 0 || prop.getInt("placed") > 0 || prop.getInt("deaths") > 0 || prop.getDouble("distance") > 0 || prop.getLong("total") > 0) {
			sql += (prop.getInt("broken") > 0) ? "broken=broken+" + prop.getInt("broken") + ", " : "";
			sql += (prop.getInt("placed") > 0) ? "placed=placed+" + prop.getInt("placed") + ", " : "";
			sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
			sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
			sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
			sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
			sql += "enter=" + now + ", seen=" + now + ", ";
			sql += ((Boolean) config.get("trackIP")) ? "ip='" + ip + "', " : "";
			sql += "total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + ", logged=1 WHERE player='" + name + "';";
			db.update(sql);
			prop.setInt("broken", 0);
			prop.setInt("placed", 0);
			prop.setInt("deaths", 0);
			prop.setInt("mobsKilled", 0);
			prop.setInt("playersKilled", 0);
			prop.setDouble("distance", 0);
			prop.setLong("enter", now);
			prop.setLong("seen", now);
			prop.setLong("total", 0);
			prop.save();
		} else {
			if (db.hasData(name)) {
				sql += "enter=" + now + ", seen=" + now;
				sql += ((Boolean) config.get("trackIP")) ? ", ip='" + ip + "'" : "";
				sql += ", logged=1 WHERE player='" + name + "';";
				db.update(sql);
				prop.setLong("enter", now);
				prop.setLong("seen", now);
				prop.save();
			} else {
				db.register(name, now, ip);
				prop.setLong("enter", now);
				prop.setLong("seen", now);
				prop.save();
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerKick(PlayerKickEvent e) {
		long now = System.currentTimeMillis();
		String name = e.getPlayer().getName();
		if (!users.containsKey(name)) {
			users.put(name, new Property(players + '/' + name + ".stats", plugin));
		}
		Property prop = users.get(name);
		String sql = "";
		// Store all data to database
		sql += "seen=" + now + ", ";
		sql += (prop.getInt("broken") > 0) ? "broken=broken+" + prop.getInt("broken") + ", " : "";
		sql += (prop.getInt("placed") > 0) ? "placed=placed+" + prop.getInt("placed") + ", " : "";
		sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
		sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
		sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
		sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
		sql += "seen=" + now + ", total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + ", logged=0 WHERE player='" + name + "';";
		db.update(sql);
		prop.setInt("broken", 0);
		prop.setInt("placed", 0);
		prop.setInt("deaths", 0);
		prop.setInt("mobsKilled", 0);
		prop.setInt("playersKilled", 0);
		prop.setDouble("distance", 0);
		prop.setLong("seen", now);
		prop.setLong("total", 0);
		prop.save();
		users.remove(name);
		if (plugin.actions.containsKey(name)) {
			plugin.actions.remove(name);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerMove(PlayerMoveEvent e) {
		if (!e.isCancelled()) {
			Player player = e.getPlayer();
			String name = player.getName();
			Vector from = e.getFrom().toVector();
			Vector to = e.getTo().toVector();
			double distance = to.distance(from);
			if (!users.containsKey(name)) {
				users.put(name, new Property(players + '/' + name + ".stats", plugin));
				plugin.actions.put(name, (action / 2));
			}
			Property prop = users.get(name);
			prop.setDouble("distance", prop.getDouble("distance") + distance);
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent e) {
		long now = System.currentTimeMillis();
		String name = e.getPlayer().getName();
		if (!users.containsKey(name)) {
			users.put(name, new Property(players + '/' + name + ".stats", plugin));
		}
		Property prop = users.get(name);
		String sql = "";
		sql += "seen=" + now + ", ";
		sql += (prop.getInt("broken") > 0) ? "broken=broken+" + prop.getInt("broken") + ", " : "";
		sql += (prop.getInt("placed") > 0) ? "placed=placed+" + prop.getInt("placed") + ", " : "";
		sql += (prop.getInt("deaths") > 0) ? "deaths=deaths+" + prop.getInt("deaths") + ", " : "";
		sql += (prop.getInt("mobsKilled") > 0) ? "mobskilled=mobskilled+" + prop.getInt("mobsKilled") + ", " : "";
		sql += (prop.getInt("playersKilled") > 0) ? "playerskilled=playerskilled+" + prop.getInt("playersKilled") + ", " : "";
		sql += (prop.getDouble("distance") > 0) ? "distance=distance+" + prop.getDouble("distance") + ", " : "";
		sql += "seen=" + now + ", total=total+" + ((prop.getLong("total") + (now - prop.getLong("seen"))) / 1000) + ", logged=0 WHERE player='" + name + "';";
		db.update(sql);
		prop.setInt("broken", 0);
		prop.setInt("placed", 0);
		prop.setInt("deaths", 0);
		prop.setInt("mobsKilled", 0);
		prop.setInt("playersKilled", 0);
		prop.setDouble("distance", 0);
		prop.setLong("seen", 0);
		prop.setLong("total", 0);
		prop.save();
		users.remove(name);
		if (plugin.actions.containsKey(name)) {
			plugin.actions.remove(name);
		}
	}
}
