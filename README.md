# McftStats
_Production Build v1.0.1_

A Bukkit plugin that allows you to keep a database of every player's stats.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

### [License](https://github.com/laCour/McftStats/blob/master/LICENSE.md)